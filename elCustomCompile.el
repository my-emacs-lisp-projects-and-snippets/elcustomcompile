;;; elCustomCompile.el --- a package that makes it easier to run and compile code using custom commands or methods souch as sending "make run<RET>" to a running tmux session -*- lexical-binding: t -*-
;; Author: Erik Lundstedt erik@lundstedt.it
;; Maintainer: Erik Lundstedt
;; Version: 0.1
;; Homepage: https://gitlab.com/erik.lundstedt/mpd-c-mode
;; Package-Requires: ((emacs "27.1")))
;;but might work on others

;; This file is not part of GNU Emacs
;; This software is free and open source under the wonderful Don't be a jerk License
;; Enjoy your free software!

;;; Commentary:
;; A package that makes it easier to run and compile code using custom commands or methods souch as sending "make run<RET>" to a running tmux session

;;; Code:

(defgroup elCustomCompile nil
"A package that makes it easier to run and compile code using custom commands or methods souch as sending \"make run<RET>\" to a running tmux session"
	:prefix "ecc-"
	:group 'tools
)

(define-minor-mode ecc-mode
  "Comment."
  :keymap (let ((map (make-sparse-keymap)))
	)
)


(define-minor-mode elCustomCompile-mode
  "Comment."
  :keymap (let ((map (make-sparse-keymap)))
	)
)

(defun ecc-generic-run-function ()
	(message projectile-root)
)
(defun ecc-generic-run-function ()
	(projectile-run-project "make run")
)
(defun ecc-generic-build-function ()
	(projectile-run-project "make build")
)
(defun ecc-generic-compile-function ()
	(projectile-run-project "make compile")
)
(defun ecc-generic-package-function ()
	(projectile-run-project "make package")
)
(defun ecc-generic-install-function ()
	(projectile-run-project "make install")
)

;;(defcustom SYMBOL STANDARD DOC &rest ARGS)
(defcustom ecc-run-function	    'ecc-generic-run-function	  "The function to run." :group 'custom-compile)
(defcustom ecc-setup-function   'ecc-generic-setup-function	  "The function to run." :group 'custom-compile)
(defcustom ecc-build-function   'ecc-generic-build-function	  "The function to run." :group 'custom-compile)
(defcustom ecc-compile-function 'ecc-generic-compile-function "The function to run." :group 'custom-compile)
(defcustom ecc-package-function 'ecc-generic-package-function "The function to run." :group 'custom-compile)
(defcustom ecc-install-function 'ecc-generic-install-function "The function to run." :group 'custom-compile)

(defun ecc-setup ()
"setup external and internal systems for use with the rest of the functions in this package."
(interactive)
(funcall ecc-setup-function)
)

(defun ecc-run	()
"Run the project."
(interactive)
(funcall ecc-run-function)
)

(defun ecc-build ()
"Build the project."
(interactive)
(funcall ecc-package-function)
)

(defun ecc-compile ()
"Compile the project."
(interactive)
(funcall ecc-package-function)
)

(defun ecc-package ()
"Package the project for easy distribution"
(interactive)
(funcall ecc-package-function)
)

(defun ecc-install ()
"Install the project to the system"
(interactive)
(funcall ecc-package-function)
)



(provide 'elCustomCompile)
;;elCustomCompile.el ends here

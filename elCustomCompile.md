
# Table of Contents

1.  [stuff needed for all emacs packages](#org8373883)
    1.  [licence header](#org1f75570)
    2.  [commentary](#org622879c)
    3.  [code](#orgb121be3)
2.  [better compile, build, package and run](#orgf2dd036)
    1.  [configuration variables that need to be set each project](#org3362f39)



<a id="org8373883"></a>

# stuff needed for all emacs packages

    ;;; elCustomCompile.el --- a package that makes it easier to run and compile code using custom commands or methods souch as sending "make run<RET>" to a running tmux session -*- lexical-binding: t -*-
    ;; Author: Erik Lundstedt erik@lundstedt.it
    ;; Maintainer: Erik Lundstedt
    ;; Version: 0.1
    ;; Homepage: https://gitlab.com/erik.lundstedt/mpd-c-mode
    ;; Package-Requires: ((emacs "27.1")))
    ;;but might work on others


<a id="org1f75570"></a>

## licence header

    ;; This file is not part of GNU Emacs
    ;; This software is free and open source under the wonderful Don't be a jerk License
    ;; Enjoy your free software!


<a id="org622879c"></a>

## commentary

    ;;; Commentary:
    ;; A package that makes it easier to run and compile code using custom commands or methods souch as sending "make run<RET>" to a running tmux session


<a id="orgb121be3"></a>

## code

    ;;; Code:


<a id="orgf2dd036"></a>

# better compile, build, package and run

    (defgroup custom-compile nil
    "A package that makes it easier to run and compile code using custom commands or methods souch as sending \"make run<RET>\" to a running tmux session"
    	:prefix "ecc-"
    	:group 'tools
    )


<a id="org3362f39"></a>

## configuration variables that need to be set each project

    (defun ecc-generic-run-function ()
    	(message projectile-root)
    )
    (defun ecc-generic-run-function ()
    	(projectile-run-project "make run")
    )
    (defun ecc-generic-build-function ()
    	(projectile-run-project "make build")
    )
    (defun ecc-generic-compile-function ()
    	(projectile-run-project "make make")
    )
    (defun ecc-generic-package-function ()
    	(projectile-run-project "make package")
    )
    (defun ecc-generic-install-function ()
    	(projectile-run-project "make install")
    )

    
    ;;(defcustom SYMBOL STANDARD DOC &rest ARGS)
    (defcustom ecc-run-function	    'ecc-generic-run-function	  "The function to run." :group 'custom-compile)
    (defcustom ecc-setup-function   'ecc-generic-setup-function	  "The function to run." :group 'custom-compile)
    (defcustom ecc-build-function   'ecc-generic-build-function	  "The function to run." :group 'custom-compile)
    (defcustom ecc-compile-function 'ecc-generic-compile-function "The function to run." :group 'custom-compile)
    (defcustom ecc-package-function 'ecc-generic-package-function "The function to run." :group 'custom-compile)
    (defcustom ecc-install-function 'ecc-generic-install-function "The function to run." :group 'custom-compile)

    (defun ecc-setup ()
    "setup external and internal systems for use with the rest of the functions in this package."
    (interactive)
    (funcall ecc-setup-function)
    )
    
    (defun ecc-run	()
    "Run the project."
    (interactive)
    (funcall ecc-run-function)
    )
    
    (defun ecc-build ()
    "Build the project."
    (interactive)
    (funcall ecc-package-function)
    )
    
    (defun ecc-compile ()
    "Compile the project."
    (interactive)
    (funcall ecc-package-function)
    )
    
    (defun ecc-package ()
    "Package the project for easy distribution"
    (interactive)
    (funcall ecc-package-function)
    )
    
    (defun ecc-install ()
    "Install the project to the system"
    (interactive)
    (funcall ecc-package-function)
    )

    

;;(map! :leader
;;	:map prog-mode
;;	(:prefix ("e" . "better-run")
;;	"e" 'ecc-package
;;	"r" 'ecc-run
;;	)
;;	)

    	(provide 'custom-compile)
    ;;elCustomCompile.el ends here

